Lightweight app server skeleton for dating apps

## Installation

 - clone the repo.
 - run `npm install` in the root of the project.
 - fire `npm run build` to build everything. You can skip this because repo distributed prebuilded
 - put your environment variables to `.env` file or
 simply copy `.env-example` stuff to `.env` for the first shot.
 - fire `npm start`

## Spawning sample database
If you have no database of users just fire
```
npm run spawn
```
after installation.
This script crawls _github_ to create user database automatically for you.

## Authorisation

#### **This authorisation flow should work over https!**
This authorisation flow should work over https, otherwise any person with wi-fi sniffer
 could stall authorisation tokens and users passwords!

#### Authorisation is done over _[OAuth2](https://tools.ietf.org/html/rfc6749)_ using *Resource Owner Password Credentials* grant type and a _[JWT](https://tools.ietf.org/html/rfc7519)_ bearer token

This grant type is used where the client is trusted by the resource owner (the user) and has a client id and
client secret known by this server.  *Trust* implies the user is willing to enter their username and password into the client, which
usually means the client is issued or approved by the some organization that owns the authorization server.

eg. the Twitter mobile app issued by Twitter, but not a third-party app what authenticates against Twitter's API.

Or similarly, Meet-matcher mobile app issued by Meet-matcher server owner, bun not a third-party app.

Mobile client use-case is:

 - The user would enter their username and password into the client. These do not need to be stored by the client (but it can use the IOS keystore).
 - The client authenticates (grant_type=password) against this server and receives an access token and refresh token. The
 client Id and client secret are also validated by this server.
 - The client uses the access token for all subsequent requests (http header Authorization: bearer <token>)
 - If the access token expires, the client requests a new one from this server (grant_type=refresh_token)
 and receives a new access token
 - The refresh token is retained by the client so the user doesn't have to login again.
 
Once the refresh token has expired the user will need to login again (or start again using credentials in the keystore).

The access token is generated using [JWT](https://tools.ietf.org/html/rfc7519). The benefits of this token are:

 - this server does not need to retain the tokens issued. It just needs to verify them.
 - the token can be passed straight through to other servers (eg. microservers) that only need to verify it (ie. they trust the signed JWT)
 - the token carries private claims that can be passed straight through to other servers (eg. user role/permission claims)

The refresh token is also using JWT, although this probably isn't necessary.


`curl` example for obtaining the token:
```
curl -X POST -d 'grant_type=password&username=thomseddon&password=nightworld&client_id=thom&client_secret=nightworld' http://localhost:3000/oauth/token
```
This request should return something like that:
```
{
  "profile_id":"cd8b985c-7701-4dc9-951e-dbb93025028d",
  "token_type":"bearer",
  "access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ0aGlzZGVtbyIsInVzZXJJZCI6IjEyMyIsImV4cCI6MTQ2MDM0Mjk0MDk5NywiaWF0IjoxNDU5OTUxMTc5fQ.Wb2BunL31Il-Cm4NrfkwCBpIlO1jTxH5oZ5jCJaeqb4",
  "expires_in":"1800",
  "refresh_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ0aGlzZGVtbyIsInVzZXJJZCI6IjEyMyIsImV4cCI6MTg2MTE2MDc0MDAwMCwiaWF0IjoxNDU5OTUxMTgwfQ.NC0xtygAJ9tINTjH-fByTXqmutBMgPMxl6cwg1yXGW4"
}
```
`access_token` here is the token for protected api access.
Also notice that JWT contains `expires_in` property,
which is very convenient to check out on client side without unnecessary bothering [OAuth server](https://github.com/oauthjs/node-oauth2-server).

Browser example link for obtaining the data protected by token:

```
http://localhost:3000/api/v1/foo?access_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ0aGlzZGVtbyIsInVzZXJJZCI6IjEyMyIsImV4cCI6MTQ2MDM0Mjk0MDk5NywiaWF0IjoxNDU5OTUxMTc5fQ.Wb2BunL31Il-Cm4NrfkwCBpIlO1jTxH5oZ5jCJaeqb4
```
`access_token` value should be replaced by one returned in the previous step.
This test api simply returns object:
```
{data: 'foo'}
```

## API

Meet-matcher is a back-end for a mobile and web applications, which relies on a server API,
that allow users from different domains (eg. companies) to create an account,
send their geolocation and match with others (like on Tinder).
The user can only see other user from the same domain.

This is a public API in REST for the app with OAuth2 as the authentication system.

The API consists of:

 - **find** - The API to find users satisfying provided terms (for now just location)
 - **get** - The API to get a list of users.
 - **match** - The API to match 2 users between themselves (like on Tinder).
 - **edit** - The API to edit the logged user (like on any social network).

Notes:
 - The API can only be used by authenticated users.
 - All possible error outputs are clear and explicit.

### Find API

#### Request

The API to find users satisfying provided terms.
For now it's just location specified as building.
Here can be easily added other terms like languages,
interests etc. if thay has _business value_

Name | Method | Desctiption
-----|--------|------------
`api/v1/find`| `GET` | Find users satisfying provided terms

#### General Parameters
Name | Data Type | default value / _required_ | Description
-----|-----------|----------------------------|------------
`access_token` | string | _required_ | The access token from OAuth2 flow.
`building` | string | _required_ | provide building as search term.

#### Sample Request

```
http://localhost:3000/api/v1/find?access_token=TOKEN_HERE&building=Spasskaya%20Tower
```

Returns the list of `profile_id` (uuid v4) of satisfying profiles.

#### Sample Response

```
[
  "cd8b985c-7701-4dc9-951e-dbb93025028d",
  "29f8205d-eeaa-467e-840a-bd6f2aa17f96",
  "5f15425d-54b2-4636-b749-1b09b4bc541d"
]
```

### Get API

The API to get a list of profiles.
If specified as parameter `ids` will be wrong uuid v4 server response with
`status` code `400` and `error` message: `"Wrong ids provided"`, it also
returns back wrong `ids` ind `data` field as array.

#### Request

Name | Method | Desctiption
-----|--------|------------
`api/v1/get`| `GET` | Retreive profile related data.

#### General Parameters
Name | Data Type | Default value / _required_| Description
-----|-----------|---------------------------|------------
`access_token` | string | _required_ | The access token from OAuth2 flow
`ids` | string | _required_ | The coma separeted profile ids for which data will be retreived.
`name` | bool | `true` | Do server need to return the name in response.
`avatar` | bool | `true` | Do server need to return the link to avatar.
`company` | bool | `true` | Do server need to return company name.
`building` | bool | `true` | Do server need to return building where user work.
`department` | bool | `true` | Do server need to return department.
`interests` | bool | `true` | Do server need to return interests.
`gender` | bool | `true` | Do server need to return gender of a user.
`languages` | bool | `true` | Do server need to return languages that user able to speak.

#### Sample Request

```
http://localhost:3000/api/v1/get?access_token=TOKEN_HERE&ids=cd8b985c-7701-4dc9-951e-dbb93025028d,c1df4872-3643-4755-8161-64c89968b5ab
```
Returns the list of profiles with acquired data.

#### Sample Response

```
[
  {
    id: "cd8b985c-7701-4dc9-951e-dbb93025028d"
    name: ["Vladislav", "Ankudinov"]
    avatar: "https://s.gravatar.com/avatar/136ec3a5fd15cd28711bc7ada0d05766?s=80",
    department: "Nuclear Unicorns",
    gender: "male",
    languages: ["Russian", "English"]
  },
  {
    id: "c1df4872-3643-4755-8161-64c89968b5ab"
    name: ["Vasilisa", "Prekrasnaya"]
    avatar: "https://s.gravatar.com/avatar/136ec3a5fd15cd28711bc7ada0d05766?s=80",
    department: "Nuclear Unicorns",
    gender: "female",
    languages: ["Russian", "English"]
  }
]
```

#### Sample Error
```
{
  status: 400
  error: "Wrong ids provided"
  data: [
    "12345",
    "54321"
  ]
}
```

### Match API

#### Request

The API to match 2 users between themselves (like on Tinder).

Name | Method | Desctiption
-----|--------|------------
`api/v1/match`| `GET` | Match 2 users

#### General Parameters
Name | Data Type | default value / _required_ | Description
-----|-----------|----------------------------|------------
`access_token` | string | _required_ | The access token from OAuth2 flow, contains `profile_id` so that we don't have to specify it in other parameters.
`beloved_id` | string | _required_ | The id of beloved profile (caller profile_id is specified in `access_token`).

#### Sample Request

```
http://localhost:3000/api/v1/match?access_token=TOKEN_HERE&beloved_id=cd8b985c-7701-4dc9-951e-dbb93025028d
```

#### Sample Response

```
{
  message: 'OK'
}
```

## Edit API

The API to edit the logged user (like on any social network).
The user allowed to edit **only his own** profile which determined by `access_token`.
Parameters provided to request will be changed,
not provided parameters stays unchanged.

### Request

Name | Method | Desctiption
-----|--------|------------
`api/v1/edit`| `POST` | Retreive profile related data.

#### General Parameters
Name | Data Type | default value / _required_ | Description
-----|-----------|----------------------------|------------
`access_token` | string | _required_ | The access token from OAuth2 flow, contains `profile_id` so that we don't have to specify it in other parameters.
`name` | string | _not required_ | Coma separated list of names, first name is _first_, last name is _last_, middle name (if present) in the _middle_, you **DON't** have to prepare free space like `,,` if there is no middle name.
`avatar` | string | _not required_ | The link to avatar.
`company` | string | _not required_ | Company name.
`building` | string | _not required_ | Building where user work.
`department` | string | _not required_ | Department where user work.
`interests` | string | _not required_ | Coma separated list of interests of the user
`gender` | string | _not required_ | Gender of a user.
`languages` | string | _not required_ | Coma separated list of languages that user able to speak.

#### Sample Request

```
http://localhost:3000/api/v1/edit?access_token=TOKEN_HERE&name=Vladislav,Ankudinov
```

#### Sample Response

```
{
  id: "cd8b985c-7701-4dc9-951e-dbb93025028d",
  name: ["Vladislav", "Ankudinov"]
}
```

# License

[MIT](./LICENSE.txt).

# Related links

[The Ultimate Guide to Mobile API Security](https://stormpath.com/blog/the-ultimate-guide-to-mobile-api-security/)

#### Standards

RFC 6749 - [The OAuth 2.0 Authorization Framework](https://tools.ietf.org/html/rfc6749)

RFC 6750 - [The OAuth 2.0 Authorization Framework: Bearer Token Usage](https://tools.ietf.org/html/rfc6750)

RFC 7515 - [JSON Web Signature (JWS)](https://tools.ietf.org/html/rfc7515)

RFC 7519 - [JSON Web Token (JWT)](https://tools.ietf.org/html/rfc7519)

RFC 7797 - [JSON Web Signature (JWS) Unencoded Payload Option](https://tools.ietf.org/html/rfc7797)

#### Node.js implementations

`node-oauth` [A simple oauth API for node.js](https://github.com/ciaranj/node-oauth)

`node-oauth2-server` [Complete, compliant and well tested module for implementing an OAuth2 server in node.js.](https://github.com/oauthjs/node-oauth2-server)

`express-oauth-server` [Complete, compliant and well tested module for implementing an OAuth2 Server/Provider with express in node.js](https://github.com/oauthjs/express-oauth-server)

`node-jws` [An implementation of JSON Web Signatures](https://github.com/brianloveswords/node-jws)

`node-jsonwebtoken` [An implementation of JSON Web Tokens](https://github.com/auth0/node-jsonwebtoken)

`express-jwt` [Middleware that validates JsonWebTokens and sets req.user](https://github.com/auth0/express-jwt)