/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(1);
	module.exports = __webpack_require__(2);


/***/ },
/* 1 */
/***/ function(module, exports) {

	module.exports = require("babel-polyfill");

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	__webpack_require__(3);
	
	var _bunyan = __webpack_require__(6);
	
	var _bunyan2 = _interopRequireDefault(_bunyan);
	
	var _express = __webpack_require__(7);
	
	var _express2 = _interopRequireDefault(_express);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var cluster = __webpack_require__(8); //import 'source-map-support/register';
	
	var numCPUs = __webpack_require__(9).cpus().length;
	// Number of workers, equals number of cores if not specified in .env
	var numWorkers = process.env.NUM_WORKERS || numCPUs;
	
	// Create logger
	var ringbuffer = new _bunyan2.default.RingBuffer({ limit: 100 });
	var log = _bunyan2.default.createLogger({
	  name: 'meet-matcher Test',
	  streams: [{
	    level: 'info',
	    stream: process.stdout
	  }, {
	    level: 'trace',
	    type: 'raw',
	    stream: ringbuffer
	  }]
	});
	
	log.info('numWorkers =', numWorkers);

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	(function () {
	  var options = {};
	  process.argv.forEach(function (val, idx, arr) {
	    var matches = val.match(/^dotenv_config_(.+)=(.+)/);
	    if (matches) {
	      options[matches[1]] = matches[2];
	    }
	  });
	
	  __webpack_require__(4).config(options);
	})();

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var fs = __webpack_require__(5);
	
	module.exports = {
	  /*
	   * Main entry point into dotenv. Allows configuration before loading .env
	   * @param {Object} options - valid options: path ('.env'), encoding ('utf8')
	   * @returns {Boolean}
	  */
	  config: function config(options) {
	    var path = '.env';
	    var encoding = 'utf8';
	    var silent = false;
	
	    if (options) {
	      if (options.silent) {
	        silent = options.silent;
	      }
	      if (options.path) {
	        path = options.path;
	      }
	      if (options.encoding) {
	        encoding = options.encoding;
	      }
	    }
	
	    try {
	      // specifying an encoding returns a string instead of a buffer
	      var parsedObj = this.parse(fs.readFileSync(path, { encoding: encoding }));
	
	      Object.keys(parsedObj).forEach(function (key) {
	        process.env[key] = process.env[key] || parsedObj[key];
	      });
	
	      return parsedObj;
	    } catch (e) {
	      if (!silent) {
	        console.error(e);
	      }
	      return false;
	    }
	  },
	
	  /*
	   * Parses a string or buffer into an object
	   * @param {String|Buffer} src - source to be parsed
	   * @returns {Object}
	  */
	  parse: function parse(src) {
	    var obj = {};
	
	    // convert Buffers before splitting into lines and processing
	    src.toString().split('\n').forEach(function (line) {
	      // matching "KEY' and 'VAL' in 'KEY=VAL'
	      var keyValueArr = line.match(/^\s*([\w\.\-]+)\s*=\s*(.*)?\s*$/);
	      // matched?
	      if (keyValueArr != null) {
	        var key = keyValueArr[1];
	
	        // default undefined or missing values to empty string
	        var value = keyValueArr[2] ? keyValueArr[2] : '';
	
	        // expand newlines in quoted values
	        var len = value ? value.length : 0;
	        if (len > 0 && value.charAt(0) === '\"' && value.charAt(len - 1) === '\"') {
	          value = value.replace(/\\n/gm, '\n');
	        }
	
	        // remove any surrounding quotes and extra spaces
	        value = value.replace(/(^['"]|['"]$)/g, '').trim();
	
	        obj[key] = value;
	      }
	    });
	
	    return obj;
	  }
	
	};
	
	module.exports.load = module.exports.config;

/***/ },
/* 5 */
/***/ function(module, exports) {

	module.exports = require("fs");

/***/ },
/* 6 */
/***/ function(module, exports) {

	module.exports = require("bunyan");

/***/ },
/* 7 */
/***/ function(module, exports) {

	module.exports = require("express");

/***/ },
/* 8 */
/***/ function(module, exports) {

	module.exports = require("cluster");

/***/ },
/* 9 */
/***/ function(module, exports) {

	module.exports = require("os");

/***/ }
/******/ ]);
//# sourceMappingURL=test.js.map