/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(1);
	module.exports = __webpack_require__(2);


/***/ },
/* 1 */
/***/ function(module, exports) {

	module.exports = require("babel-polyfill");

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	__webpack_require__(3);
	
	__webpack_require__(8);
	
	var _bunyan = __webpack_require__(10);
	
	var _bunyan2 = _interopRequireDefault(_bunyan);
	
	var _express = __webpack_require__(11);
	
	var _express2 = _interopRequireDefault(_express);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	// Create logger
	var ringbuffer = new _bunyan2.default.RingBuffer({ limit: 100 });
	var log = _bunyan2.default.createLogger({
	  name: 'meet-matcher Server',
	  streams: [{
	    level: 'info',
	    stream: process.stdout
	  }, {
	    level: 'trace',
	    type: 'raw',
	    stream: ringbuffer
	  }]
	});
	
	var bodyParser = __webpack_require__(12);
	var oauthserver = __webpack_require__(13);
	var memorystore = __webpack_require__(14);
	
	var app = (0, _express2.default)();
	app.set('port', process.env.PORT);
	if (!app.get('port')) throw new Error('PORT not specified!');
	
	app.use(bodyParser.urlencoded({ extended: true }));
	app.use(bodyParser.json());
	
	app.oauth = oauthserver({
	  model: memorystore,
	  grants: ['password', 'refresh_token'],
	  debug: true,
	  // expiry time in seconds, consistent with JWT setting in model.js
	  accessTokenLifetime: memorystore.JWT_ACCESS_TOKEN_EXPIRY_SECONDS,
	  // expiry time in seconds, consistent with JWT setting in model.js
	  refreshTokenLifetime: memorystore.JWT_REFRESH_TOKEN_EXPIRY_SECONDS
	});
	
	app.all('/oauth/token', app.oauth.grant());
	
	// simple protected api example
	app.get('/api/v1/foo', app.oauth.authorise(), function (req, res) {
	  log.info('access to foo api');
	  return res.json({ data: 'foo' });
	});
	
	app.use(app.oauth.errorHandler());
	
	app.listen(app.get('port'), function (err) {
	  if (err) {
	    return log.error('Starting server error =', err);
	  }
	  log.info('Server listening on port =', app.get('port'));
	});

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	__webpack_require__(4).install();

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };
	
	var SourceMapConsumer = __webpack_require__(5).SourceMapConsumer;
	var path = __webpack_require__(6);
	var fs = __webpack_require__(7);
	
	// Only install once if called multiple times
	var errorFormatterInstalled = false;
	var uncaughtShimInstalled = false;
	
	// If true, the caches are reset before a stack trace formatting operation
	var emptyCacheBetweenOperations = false;
	
	// Supports {browser, node, auto}
	var environment = "auto";
	
	// Maps a file path to a string containing the file contents
	var fileContentsCache = {};
	
	// Maps a file path to a source map for that file
	var sourceMapCache = {};
	
	// Regex for detecting source maps
	var reSourceMap = /^data:application\/json[^,]+base64,/;
	
	// Priority list of retrieve handlers
	var retrieveFileHandlers = [];
	var retrieveMapHandlers = [];
	
	function isInBrowser() {
	  if (environment === "browser") return true;
	  if (environment === "node") return false;
	  return typeof window !== 'undefined' && typeof XMLHttpRequest === 'function';
	}
	
	function hasGlobalProcessEventEmitter() {
	  return (typeof process === 'undefined' ? 'undefined' : _typeof(process)) === 'object' && process !== null && typeof process.on === 'function';
	}
	
	function handlerExec(list) {
	  return function (arg) {
	    for (var i = 0; i < list.length; i++) {
	      var ret = list[i](arg);
	      if (ret) {
	        return ret;
	      }
	    }
	    return null;
	  };
	}
	
	var retrieveFile = handlerExec(retrieveFileHandlers);
	
	retrieveFileHandlers.push(function (path) {
	  // Trim the path to make sure there is no extra whitespace.
	  path = path.trim();
	  if (path in fileContentsCache) {
	    return fileContentsCache[path];
	  }
	
	  try {
	    // Use SJAX if we are in the browser
	    if (isInBrowser()) {
	      var xhr = new XMLHttpRequest();
	      xhr.open('GET', path, false);
	      xhr.send(null);
	      var contents = null;
	      if (xhr.readyState === 4 && xhr.status === 200) {
	        contents = xhr.responseText;
	      }
	    }
	
	    // Otherwise, use the filesystem
	    else {
	        var contents = fs.readFileSync(path, 'utf8');
	      }
	  } catch (e) {
	    var contents = null;
	  }
	
	  return fileContentsCache[path] = contents;
	});
	
	// Support URLs relative to a directory, but be careful about a protocol prefix
	// in case we are in the browser (i.e. directories may start with "http://")
	function supportRelativeURL(file, url) {
	  if (!file) return url;
	  var dir = path.dirname(file);
	  var match = /^\w+:\/\/[^\/]*/.exec(dir);
	  var protocol = match ? match[0] : '';
	  return protocol + path.resolve(dir.slice(protocol.length), url);
	}
	
	function retrieveSourceMapURL(source) {
	  var fileData;
	
	  if (isInBrowser()) {
	    var xhr = new XMLHttpRequest();
	    xhr.open('GET', source, false);
	    xhr.send(null);
	    fileData = xhr.readyState === 4 ? xhr.responseText : null;
	
	    // Support providing a sourceMappingURL via the SourceMap header
	    var sourceMapHeader = xhr.getResponseHeader("SourceMap") || xhr.getResponseHeader("X-SourceMap");
	    if (sourceMapHeader) {
	      return sourceMapHeader;
	    }
	  }
	
	  // Get the URL of the source map
	  fileData = retrieveFile(source);
	  //        //# sourceMappingURL=foo.js.map                       /*# sourceMappingURL=foo.js.map */
	  var re = /(?:\/\/[@#][ \t]+sourceMappingURL=([^\s'"]+?)[ \t]*$)|(?:\/\*[@#][ \t]+sourceMappingURL=([^\*]+?)[ \t]*(?:\*\/)[ \t]*$)/mg;
	  // Keep executing the search to find the *last* sourceMappingURL to avoid
	  // picking up sourceMappingURLs from comments, strings, etc.
	  var lastMatch, match;
	  while (match = re.exec(fileData)) {
	    lastMatch = match;
	  }if (!lastMatch) return null;
	  return lastMatch[1];
	};
	
	// Can be overridden by the retrieveSourceMap option to install. Takes a
	// generated source filename; returns a {map, optional url} object, or null if
	// there is no source map.  The map field may be either a string or the parsed
	// JSON object (ie, it must be a valid argument to the SourceMapConsumer
	// constructor).
	var retrieveSourceMap = handlerExec(retrieveMapHandlers);
	retrieveMapHandlers.push(function (source) {
	  var sourceMappingURL = retrieveSourceMapURL(source);
	  if (!sourceMappingURL) return null;
	
	  // Read the contents of the source map
	  var sourceMapData;
	  if (reSourceMap.test(sourceMappingURL)) {
	    // Support source map URL as a data url
	    var rawData = sourceMappingURL.slice(sourceMappingURL.indexOf(',') + 1);
	    sourceMapData = new Buffer(rawData, "base64").toString();
	    sourceMappingURL = null;
	  } else {
	    // Support source map URLs relative to the source URL
	    sourceMappingURL = supportRelativeURL(source, sourceMappingURL);
	    sourceMapData = retrieveFile(sourceMappingURL);
	  }
	
	  if (!sourceMapData) {
	    return null;
	  }
	
	  return {
	    url: sourceMappingURL,
	    map: sourceMapData
	  };
	});
	
	function mapSourcePosition(position) {
	  var sourceMap = sourceMapCache[position.source];
	  if (!sourceMap) {
	    // Call the (overrideable) retrieveSourceMap function to get the source map.
	    var urlAndMap = retrieveSourceMap(position.source);
	    if (urlAndMap) {
	      sourceMap = sourceMapCache[position.source] = {
	        url: urlAndMap.url,
	        map: new SourceMapConsumer(urlAndMap.map)
	      };
	
	      // Load all sources stored inline with the source map into the file cache
	      // to pretend like they are already loaded. They may not exist on disk.
	      if (sourceMap.map.sourcesContent) {
	        sourceMap.map.sources.forEach(function (source, i) {
	          var contents = sourceMap.map.sourcesContent[i];
	          if (contents) {
	            var url = supportRelativeURL(sourceMap.url, source);
	            fileContentsCache[url] = contents;
	          }
	        });
	      }
	    } else {
	      sourceMap = sourceMapCache[position.source] = {
	        url: null,
	        map: null
	      };
	    }
	  }
	
	  // Resolve the source URL relative to the URL of the source map
	  if (sourceMap && sourceMap.map) {
	    var originalPosition = sourceMap.map.originalPositionFor(position);
	
	    // Only return the original position if a matching line was found. If no
	    // matching line is found then we return position instead, which will cause
	    // the stack trace to print the path and line for the compiled file. It is
	    // better to give a precise location in the compiled file than a vague
	    // location in the original file.
	    if (originalPosition.source !== null) {
	      originalPosition.source = supportRelativeURL(sourceMap.url, originalPosition.source);
	      return originalPosition;
	    }
	  }
	
	  return position;
	}
	
	// Parses code generated by FormatEvalOrigin(), a function inside V8:
	// https://code.google.com/p/v8/source/browse/trunk/src/messages.js
	function mapEvalOrigin(origin) {
	  // Most eval() calls are in this format
	  var match = /^eval at ([^(]+) \((.+):(\d+):(\d+)\)$/.exec(origin);
	  if (match) {
	    var position = mapSourcePosition({
	      source: match[2],
	      line: match[3],
	      column: match[4] - 1
	    });
	    return 'eval at ' + match[1] + ' (' + position.source + ':' + position.line + ':' + (position.column + 1) + ')';
	  }
	
	  // Parse nested eval() calls using recursion
	  match = /^eval at ([^(]+) \((.+)\)$/.exec(origin);
	  if (match) {
	    return 'eval at ' + match[1] + ' (' + mapEvalOrigin(match[2]) + ')';
	  }
	
	  // Make sure we still return useful information if we didn't find anything
	  return origin;
	}
	
	// This is copied almost verbatim from the V8 source code at
	// https://code.google.com/p/v8/source/browse/trunk/src/messages.js. The
	// implementation of wrapCallSite() used to just forward to the actual source
	// code of CallSite.prototype.toString but unfortunately a new release of V8
	// did something to the prototype chain and broke the shim. The only fix I
	// could find was copy/paste.
	function CallSiteToString() {
	  var fileName;
	  var fileLocation = "";
	  if (this.isNative()) {
	    fileLocation = "native";
	  } else {
	    fileName = this.getScriptNameOrSourceURL();
	    if (!fileName && this.isEval()) {
	      fileLocation = this.getEvalOrigin();
	      fileLocation += ", "; // Expecting source position to follow.
	    }
	
	    if (fileName) {
	      fileLocation += fileName;
	    } else {
	      // Source code does not originate from a file and is not native, but we
	      // can still get the source position inside the source string, e.g. in
	      // an eval string.
	      fileLocation += "<anonymous>";
	    }
	    var lineNumber = this.getLineNumber();
	    if (lineNumber != null) {
	      fileLocation += ":" + lineNumber;
	      var columnNumber = this.getColumnNumber();
	      if (columnNumber) {
	        fileLocation += ":" + columnNumber;
	      }
	    }
	  }
	
	  var line = "";
	  var functionName = this.getFunctionName();
	  var addSuffix = true;
	  var isConstructor = this.isConstructor();
	  var isMethodCall = !(this.isToplevel() || isConstructor);
	  if (isMethodCall) {
	    var typeName = this.getTypeName();
	    var methodName = this.getMethodName();
	    if (functionName) {
	      if (typeName && functionName.indexOf(typeName) != 0) {
	        line += typeName + ".";
	      }
	      line += functionName;
	      if (methodName && functionName.indexOf("." + methodName) != functionName.length - methodName.length - 1) {
	        line += " [as " + methodName + "]";
	      }
	    } else {
	      line += typeName + "." + (methodName || "<anonymous>");
	    }
	  } else if (isConstructor) {
	    line += "new " + (functionName || "<anonymous>");
	  } else if (functionName) {
	    line += functionName;
	  } else {
	    line += fileLocation;
	    addSuffix = false;
	  }
	  if (addSuffix) {
	    line += " (" + fileLocation + ")";
	  }
	  return line;
	}
	
	function cloneCallSite(frame) {
	  var object = {};
	  Object.getOwnPropertyNames(Object.getPrototypeOf(frame)).forEach(function (name) {
	    object[name] = /^(?:is|get)/.test(name) ? function () {
	      return frame[name].call(frame);
	    } : frame[name];
	  });
	  object.toString = CallSiteToString;
	  return object;
	}
	
	function wrapCallSite(frame) {
	  // Most call sites will return the source file from getFileName(), but code
	  // passed to eval() ending in "//# sourceURL=..." will return the source file
	  // from getScriptNameOrSourceURL() instead
	  var source = frame.getFileName() || frame.getScriptNameOrSourceURL();
	  if (source) {
	    var line = frame.getLineNumber();
	    var column = frame.getColumnNumber() - 1;
	
	    // Fix position in Node where some (internal) code is prepended.
	    // See https://github.com/evanw/node-source-map-support/issues/36
	    if (line === 1 && !isInBrowser() && !frame.isEval()) {
	      column -= 62;
	    }
	
	    var position = mapSourcePosition({
	      source: source,
	      line: line,
	      column: column
	    });
	    frame = cloneCallSite(frame);
	    frame.getFileName = function () {
	      return position.source;
	    };
	    frame.getLineNumber = function () {
	      return position.line;
	    };
	    frame.getColumnNumber = function () {
	      return position.column + 1;
	    };
	    frame.getScriptNameOrSourceURL = function () {
	      return position.source;
	    };
	    return frame;
	  }
	
	  // Code called using eval() needs special handling
	  var origin = frame.isEval() && frame.getEvalOrigin();
	  if (origin) {
	    origin = mapEvalOrigin(origin);
	    frame = cloneCallSite(frame);
	    frame.getEvalOrigin = function () {
	      return origin;
	    };
	    return frame;
	  }
	
	  // If we get here then we were unable to change the source position
	  return frame;
	}
	
	// This function is part of the V8 stack trace API, for more info see:
	// http://code.google.com/p/v8/wiki/JavaScriptStackTraceApi
	function prepareStackTrace(error, stack) {
	  if (emptyCacheBetweenOperations) {
	    fileContentsCache = {};
	    sourceMapCache = {};
	  }
	
	  return error + stack.map(function (frame) {
	    return '\n    at ' + wrapCallSite(frame);
	  }).join('');
	}
	
	// Generate position and snippet of original source with pointer
	function getErrorSource(error) {
	  var match = /\n    at [^(]+ \((.*):(\d+):(\d+)\)/.exec(error.stack);
	  if (match) {
	    var source = match[1];
	    var line = +match[2];
	    var column = +match[3];
	
	    // Support the inline sourceContents inside the source map
	    var contents = fileContentsCache[source];
	
	    // Support files on disk
	    if (!contents && fs.existsSync(source)) {
	      contents = fs.readFileSync(source, 'utf8');
	    }
	
	    // Format the line from the original source code like node does
	    if (contents) {
	      var code = contents.split(/(?:\r\n|\r|\n)/)[line - 1];
	      if (code) {
	        return source + ':' + line + '\n' + code + '\n' + new Array(column).join(' ') + '^';
	      }
	    }
	  }
	  return null;
	}
	
	function printErrorAndExit(error) {
	  var source = getErrorSource(error);
	
	  if (source) {
	    console.error();
	    console.error(source);
	  }
	
	  console.error(error.stack);
	  process.exit(1);
	}
	
	function shimEmitUncaughtException() {
	  var origEmit = process.emit;
	
	  process.emit = function (type) {
	    if (type === 'uncaughtException') {
	      var hasStack = arguments[1] && arguments[1].stack;
	      var hasListeners = this.listeners(type).length > 0;
	
	      if (hasStack && !hasListeners) {
	        return printErrorAndExit(arguments[1]);
	      }
	    }
	
	    return origEmit.apply(this, arguments);
	  };
	}
	
	exports.wrapCallSite = wrapCallSite;
	exports.getErrorSource = getErrorSource;
	exports.mapSourcePosition = mapSourcePosition;
	exports.retrieveSourceMap = retrieveSourceMap;
	
	exports.install = function (options) {
	  options = options || {};
	
	  if (options.environment) {
	    environment = options.environment;
	    if (["node", "browser", "auto"].indexOf(environment) === -1) {
	      throw new Error("environment " + environment + " was unknown. Available options are {auto, browser, node}");
	    }
	  }
	
	  // Allow sources to be found by methods other than reading the files
	  // directly from disk.
	  if (options.retrieveFile) {
	    if (options.overrideRetrieveFile) {
	      retrieveFileHandlers.length = 0;
	    }
	
	    retrieveFileHandlers.unshift(options.retrieveFile);
	  }
	
	  // Allow source maps to be found by methods other than reading the files
	  // directly from disk.
	  if (options.retrieveSourceMap) {
	    if (options.overrideRetrieveSourceMap) {
	      retrieveMapHandlers.length = 0;
	    }
	
	    retrieveMapHandlers.unshift(options.retrieveSourceMap);
	  }
	
	  // Configure options
	  if (!emptyCacheBetweenOperations) {
	    emptyCacheBetweenOperations = 'emptyCacheBetweenOperations' in options ? options.emptyCacheBetweenOperations : false;
	  }
	
	  // Install the error reformatter
	  if (!errorFormatterInstalled) {
	    errorFormatterInstalled = true;
	    Error.prepareStackTrace = prepareStackTrace;
	  }
	
	  if (!uncaughtShimInstalled) {
	    var installHandler = 'handleUncaughtExceptions' in options ? options.handleUncaughtExceptions : true;
	
	    // Provide the option to not install the uncaught exception handler. This is
	    // to support other uncaught exception handlers (in test frameworks, for
	    // example). If this handler is not installed and there are no other uncaught
	    // exception handlers, uncaught exceptions will be caught by node's built-in
	    // exception handler and the process will still be terminated. However, the
	    // generated JavaScript code will be shown above the stack trace instead of
	    // the original source code.
	    if (installHandler && hasGlobalProcessEventEmitter()) {
	      uncaughtShimInstalled = true;
	      shimEmitUncaughtException();
	    }
	  }
	};

/***/ },
/* 5 */
/***/ function(module, exports) {

	module.exports = require("source-map");

/***/ },
/* 6 */
/***/ function(module, exports) {

	module.exports = require("path");

/***/ },
/* 7 */
/***/ function(module, exports) {

	module.exports = require("fs");

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	(function () {
	  var options = {};
	  process.argv.forEach(function (val, idx, arr) {
	    var matches = val.match(/^dotenv_config_(.+)=(.+)/);
	    if (matches) {
	      options[matches[1]] = matches[2];
	    }
	  });
	
	  __webpack_require__(9).config(options);
	})();

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var fs = __webpack_require__(7);
	
	module.exports = {
	  /*
	   * Main entry point into dotenv. Allows configuration before loading .env
	   * @param {Object} options - valid options: path ('.env'), encoding ('utf8')
	   * @returns {Boolean}
	  */
	  config: function config(options) {
	    var path = '.env';
	    var encoding = 'utf8';
	    var silent = false;
	
	    if (options) {
	      if (options.silent) {
	        silent = options.silent;
	      }
	      if (options.path) {
	        path = options.path;
	      }
	      if (options.encoding) {
	        encoding = options.encoding;
	      }
	    }
	
	    try {
	      // specifying an encoding returns a string instead of a buffer
	      var parsedObj = this.parse(fs.readFileSync(path, { encoding: encoding }));
	
	      Object.keys(parsedObj).forEach(function (key) {
	        process.env[key] = process.env[key] || parsedObj[key];
	      });
	
	      return parsedObj;
	    } catch (e) {
	      if (!silent) {
	        console.error(e);
	      }
	      return false;
	    }
	  },
	
	  /*
	   * Parses a string or buffer into an object
	   * @param {String|Buffer} src - source to be parsed
	   * @returns {Object}
	  */
	  parse: function parse(src) {
	    var obj = {};
	
	    // convert Buffers before splitting into lines and processing
	    src.toString().split('\n').forEach(function (line) {
	      // matching "KEY' and 'VAL' in 'KEY=VAL'
	      var keyValueArr = line.match(/^\s*([\w\.\-]+)\s*=\s*(.*)?\s*$/);
	      // matched?
	      if (keyValueArr != null) {
	        var key = keyValueArr[1];
	
	        // default undefined or missing values to empty string
	        var value = keyValueArr[2] ? keyValueArr[2] : '';
	
	        // expand newlines in quoted values
	        var len = value ? value.length : 0;
	        if (len > 0 && value.charAt(0) === '\"' && value.charAt(len - 1) === '\"') {
	          value = value.replace(/\\n/gm, '\n');
	        }
	
	        // remove any surrounding quotes and extra spaces
	        value = value.replace(/(^['"]|['"]$)/g, '').trim();
	
	        obj[key] = value;
	      }
	    });
	
	    return obj;
	  }
	
	};
	
	module.exports.load = module.exports.config;

/***/ },
/* 10 */
/***/ function(module, exports) {

	module.exports = require("bunyan");

/***/ },
/* 11 */
/***/ function(module, exports) {

	module.exports = require("express");

/***/ },
/* 12 */
/***/ function(module, exports) {

	module.exports = require("body-parser");

/***/ },
/* 13 */
/***/ function(module, exports) {

	module.exports = require("oauth2-server");

/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	__webpack_require__(8);
	
	var JWT = __webpack_require__(15);
	
	var model = module.exports;
	
	var JWT_ISSUER = process.env.JWT_ISSUER;
	var JWT_SECRET_FOR_ACCESS_TOKEN = process.env.JWT_SECRET_FOR_ACCESS_TOKEN;
	var JWT_SECRET_FOR_REFRESH_TOKEN = process.env.JWT_SECRET_FOR_REFRESH_TOKEN;
	if (!JWT_ISSUER) throw new Error('JWT_ISSUER not specified!');
	if (!JWT_SECRET_FOR_ACCESS_TOKEN) throw new Error('JWT_SECRET_FOR_ACCESS_TOKEN not specified!');
	if (!JWT_SECRET_FOR_REFRESH_TOKEN) throw new Error('JWT_SECRET_FOR_REFRESH_TOKEN not specified!');
	
	// the expiry times should be consistent between the oauth2-server settings
	// and the JWT settings (not essential, but makes sense)
	model.JWT_ACCESS_TOKEN_EXPIRY_SECONDS = process.env.JWT_ACCESS_TOKEN_EXPIRY_SECONDS || 1800; // 30 minutes
	model.JWT_REFRESH_TOKEN_EXPIRY_SECONDS = process.env.JWT_REFRESH_TOKEN_EXPIRY_SECONDS || 1209600; // 14 days
	
	// In-memory datastores
	var oauthClients = [{
	  clientId: 'thom',
	  clientSecret: 'nightworld',
	  redirectUri: ''
	}];
	
	// key is grant_type
	// value is the array of authorized clientId's
	var authorizedClientIds = {
	  password: ['thom'],
	  refresh_token: ['thom']
	};
	
	// current registered users
	var users = [{
	  id: '123',
	  username: 'thomseddon',
	  password: 'nightworld'
	}];
	
	// Functions required to implement the model for oauth2-server
	
	// generateToken
	// This generateToken implementation generates a token with JWT.
	// the token output is the Base64 encoded string.
	model.generateToken = function (type, req, callback) {
	  var token;
	  var secret;
	  var user = req.user;
	  var exp = new Date();
	  var payload = {
	    // public claims
	    iss: JWT_ISSUER, // issuer
	    // exp: exp,       // the expiry date is set below - expiry depends on type
	    // jti: '',        // unique id for this token - needed if we keep an store of issued tokens?
	
	    // private claims
	    userId: user.id
	  };
	  var options = {
	    algorithms: ['HS256'] // HMAC using SHA-256 hash algorithm
	  };
	
	  if (type === 'accessToken') {
	    secret = JWT_SECRET_FOR_ACCESS_TOKEN;
	    exp.setSeconds(exp.getSeconds() + model.JWT_ACCESS_TOKEN_EXPIRY_SECONDS);
	  } else {
	    secret = JWT_SECRET_FOR_REFRESH_TOKEN;
	    exp.setSeconds(exp.getSeconds() + model.JWT_REFRESH_TOKEN_EXPIRY_SECONDS);
	  }
	  payload.exp = exp.getTime();
	
	  token = JWT.sign(payload, secret, options);
	
	  callback(false, token);
	};
	
	// The bearer token is a JWT, so we decrypt and verify it. We get a reference to the
	// user in this function which oauth2-server puts into the req object
	model.getAccessToken = function (bearerToken, callback) {
	
	  return JWT.verify(bearerToken, JWT_SECRET_FOR_ACCESS_TOKEN, function (err, decoded) {
	
	    if (err) {
	      return callback(err, false); // the err contains JWT error data
	    }
	
	    // other verifications could be performed here
	    // eg. that the jti is valid
	
	    // we could pass the payload straight out we use an object with the
	    // mandatory keys expected by oauth2-server, plus any other private
	    // claims that are useful
	    return callback(false, {
	      expires: new Date(decoded.exp),
	      user: getUserById(decoded.userId)
	    });
	  });
	};
	
	// As we're using JWT there's no need to store the token after it's generated
	model.saveAccessToken = function (accessToken, clientId, expires, userId, callback) {
	  return callback(false);
	};
	
	// The bearer token is a JWT, so we decrypt and verify it. We get a reference to the
	// user in this function which oauth2-server puts into the req object
	model.getRefreshToken = function (bearerToken, callback) {
	  return JWT.verify(bearerToken, JWT_SECRET_FOR_REFRESH_TOKEN, function (err, decoded) {
	
	    if (err) {
	      return callback(err, false);
	    }
	
	    // other verifications could be performed here
	    // eg. that the jti is valid
	
	    // instead of passing the payload straight out we use an object with the
	    // mandatory keys expected by oauth2-server plus any other private
	    // claims that are useful
	    return callback(false, {
	      expires: new Date(decoded.exp),
	      user: getUserById(decoded.userId)
	    });
	  });
	};
	
	// required for grant_type=refresh_token
	// As we're using JWT there's no need to store the token after it's generated
	model.saveRefreshToken = function (refreshToken, clientId, expires, userId, callback) {
	  return callback(false);
	};
	
	// authenticate the client specified by id and secret
	model.getClient = function (clientId, clientSecret, callback) {
	  for (var i = 0, len = oauthClients.length; i < len; i++) {
	    var elem = oauthClients[i];
	    if (elem.clientId === clientId && (clientSecret === null || elem.clientSecret === clientSecret)) {
	      return callback(false, elem);
	    }
	  }
	  callback(false, false);
	};
	
	// determine whether the client is allowed the requested grant type
	model.grantTypeAllowed = function (clientId, grantType, callback) {
	  callback(false, authorizedClientIds[grantType] && authorizedClientIds[grantType].indexOf(clientId.toLowerCase()) >= 0);
	};
	
	// authenticate a user
	// for grant_type password
	model.getUser = function (username, password, callback) {
	  for (var i = 0, len = users.length; i < len; i++) {
	    var elem = users[i];
	    if (elem.username === username && elem.password === password) {
	      return callback(false, elem);
	    }
	  }
	  callback(false, false);
	};
	
	var getUserById = function getUserById(userId) {
	  for (var i = 0, len = users.length; i < len; i++) {
	    var elem = users[i];
	    if (elem.id === userId) {
	      return elem;
	    }
	  }
	  return null;
	};
	
	// for grant_type client_credentials
	// given client credentials
	//   authenticate client
	//   lookup user
	//   return that user...
	//     oauth replies with access token and renewal token
	//model.getUserFromClient = function(clientId, clientSecret, callback) {
	//
	//};

/***/ },
/* 15 */
/***/ function(module, exports) {

	module.exports = require("jsonwebtoken");

/***/ }
/******/ ]);
//# sourceMappingURL=server.js.map