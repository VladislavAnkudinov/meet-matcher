//var webpack = require('webpack');
var path = require('path');
var fs = require('fs');

var nodeModules = {};
fs.readdirSync('node_modules')
  .filter(function (x) {
    return ['.bin'].indexOf(x) === -1;
  })
  .forEach(function (mod) {
    nodeModules[mod] = 'commonjs ' + mod;
  });

var jobModule = {
  loaders: [
    {
      test: /\.js$/,
      loader: 'babel',
      query: {
        presets: ['es2015', 'stage-0']
      }
    },
    {
      loader: 'json-loader',
      test: /\.json$/
    }
  ]
};

var jobs = fs.readdirSync(path.join('src', 'tests'))
  .map(function (filename) {
    return {
      target: 'node',
      devtool: 'source-map',
      entry: ['babel-polyfill', './' + path.join('src', 'tests', filename)],
      output: {
        path: path.join(__dirname, 'bld', 'tests'),
        filename: filename
      },
      module: jobModule,
      externals: nodeModules
    };
  });

jobs.push({
  target: 'node',
  devtool: 'source-map',
  entry: ['babel-polyfill', './' + path.join('src', 'client', 'client.js')],
  output: {
    path: path.join(__dirname, 'bld'),
    filename: 'client.js'
  },
  module: jobModule,
  externals: nodeModules
});

jobs.push({
  target: 'node',
  devtool: 'source-map',
  entry: ['babel-polyfill', './' + path.join('src', 'server', 'server.js')],
  output: {
    path: path.join(__dirname, 'bld'),
    filename: 'server.js'
  },
  module: jobModule,
  externals: nodeModules
});

jobs.push({
  target: 'node',
  devtool: 'source-map',
  entry: ['babel-polyfill', './' + path.join('src', 'spawner', 'spawner.js')],
  output: {
    path: path.join(__dirname, 'bld'),
    filename: 'spawner.js'
  },
  module: jobModule,
  externals: nodeModules
});

module.exports = jobs;
