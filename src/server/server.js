import 'source-map-support/register';
import 'dotenv/config';
import bunyan from 'bunyan';
import express from 'express';

// Create logger
let ringbuffer = new bunyan.RingBuffer({limit: 100});
let log = bunyan.createLogger({
  name: 'meet-matcher Server',
  streams: [
    {
      level: 'info',
      stream: process.stdout
    },
    {
      level: 'trace',
      type: 'raw',
      stream: ringbuffer
    }
  ]
});

var bodyParser = require('body-parser');
var oauthserver = require('oauth2-server');
var memorystore = require('./model.js');

var app = express();
app.set('port', process.env.PORT);
if (!app.get('port')) throw new Error('PORT not specified!');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.oauth = oauthserver({
  model: memorystore,
  grants: ['password', 'refresh_token'],
  debug: true,
  // expiry time in seconds, consistent with JWT setting in model.js
  accessTokenLifetime: memorystore.JWT_ACCESS_TOKEN_EXPIRY_SECONDS,
  // expiry time in seconds, consistent with JWT setting in model.js
  refreshTokenLifetime: memorystore.JWT_REFRESH_TOKEN_EXPIRY_SECONDS
});

app.all('/oauth/token', app.oauth.grant());

// simple protected api example
app.get('/api/v1/foo', app.oauth.authorise(), function (req, res) {
  log.info('access to foo api');
  return res.json({data: 'foo'})
});

app.use(app.oauth.errorHandler());

app.listen(app.get('port'), (err)=> {
  if (err) {
    return log.error('Starting server error =', err);
  }
  log.info('Server listening on port =', app.get('port'));
});
