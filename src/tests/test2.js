//import 'source-map-support/register';
import 'dotenv/config';
import bunyan from 'bunyan';
import express from 'express';

const cluster = require('cluster');
const numCPUs = require('os').cpus().length;
// Number of workers, equals number of cores if not specified in .env
const numWorkers = process.env.NUM_WORKERS || numCPUs;

// Create logger
let ringbuffer = new bunyan.RingBuffer({limit: 100});
let log = bunyan.createLogger({
  name: `meet-matcher Test2`,
  streams: [
    {
      level: 'info',
      stream: process.stdout
    },
    {
      level: 'trace',
      type: 'raw',
      stream: ringbuffer
    }
  ]
});

log.info('numWorkers =', numWorkers);
