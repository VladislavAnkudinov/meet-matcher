import 'source-map-support/register';
import 'dotenv/config';
import bunyan from 'bunyan';
import { MongoClient } from 'mongodb';

var rp = require('request-promise');
var cheerio = require('cheerio'); // Basically jQuery for node.js
var _ = require('lodash');
var bb = require('bluebird');
var uuid = require('node-uuid');
const bcrypt = require('bcrypt-nodejs');
bb.promisifyAll(bcrypt);

let ringbuffer = new bunyan.RingBuffer({limit: 100});
let log = bunyan.createLogger({
  name: 'meet-matcher Spawner',
  streams: [
    {
      level: 'info',
      stream: process.stdout
    },
    {
      level: 'trace',
      type: 'raw',
      stream: ringbuffer
    }
  ]
});

let link = (org) => `https://github.com/orgs/${org}/people`;
let linkPage = (org, page)=> `https://github.com/orgs/${org}/people?page=${page}`;
let companies = [
  'nodejs',
  'babel',
  'reactjs',
  'angular'
];
let buildings = [
  'Borovitskaya Tower',
  'Blagoveschenskaya Tower',
  'Vodovzvodnaya Tower',
  'Taynitskaya Tower',
  'Beklemishevskaya Tower',
  'First Unnamed Tower',
  'Second Unnamed Tower',
  'Petrovskaya Tower',
  'Konstantino-Eleninskaya Tower',
  'Nabatnaya Tower',
  'Tsarskaya Tower',
  'Spasskaya Tower',
  'Senatskaya Tower',
  'Nikolskaya Tower',
  'Middle Arsenalnaya Tower',
  'Corner Arsenalnaya Tower',
  'Komendantskaya Tower',
  'Troitskaya Tower',
  'Oruzheynaya Tower',
  'Kutafya Tower'
];
let departments = [
  'Production',
  'Research and Development',
  'Purchasing',
  'Marketing',
  'Sales',
  'Human Resource Management',
  'Accounting and Finance',
  'Nuclear Unicorns'
];
let interests = ['piano', 'snowboard', 'photo', 'motorcycles', 'computers'];
let genders = ['male', 'female', 'lesbian', 'gey', 'bisexual', 'trans', 'asexual', 'married', 'other'];
let languages = ['English', 'Swedish', 'Cebuano', 'German', 'Dutch', 'French', 'Russian', 'Chinese'];

let companiesProfiles = bb.mapSeries(companies, (company) => {
  return rp({
    uri: link(company),
    transform(body) {
      return cheerio.load(body);
    }
  }).then(function ($) {
    let cnt = parseInt($('.counter').html());
    let perPage = $('.table-list-cell.member-info').length;
    let pages = Math.ceil(cnt / perPage);
    let companiesProfiles = [bb.resolve($)];
    log.info(`company = ${company}, cnt = ${cnt}`);
    log.info(`company = ${company}, perPage = ${perPage}`);
    log.info(`company = ${company}, pages = ${pages}`);
    if (pages > 1) {
      for (let i = 2; i <= pages; i++) {
        ((i)=> {
          companiesProfiles.push(bb.resolve(companiesProfiles[i - 1]).then(()=> {
            return rp({
              uri: linkPage(company, i),
              transform(body) {
                return cheerio.load(body);
              }
            })
          }));
        })(i)
      }
    }
    return Promise.all(companiesProfiles);
  });
});

let profiles = bb.mapSeries(companiesProfiles, (pages, index)=> {
    let profiles = pages.map(($)=> {
      let onPage = $('.table-list-cell.member-info').length;
      let profiles = [];
      for (let i = 0; i < onPage; i++) {
        let name = ($('.table-list-cell.member-info span').eq(i).html() || '').split(' ');
        let id = uuid.v4();
        log.info('Populating user, name =', name);
        profiles.push({
          _id: id,
          id: id,
          email: name.join('').toLowerCase() + '@example.com',
          password: bcrypt.hashSync(name.join('').toLowerCase(), bcrypt.genSaltSync()),
          name: name,
          avatar: $('.table-list-cell.member-info img').eq(i).attr().src,
          company: companies[index],
          building: buildings[Math.floor(Math.random() * buildings.length)],
          department: departments[Math.floor(Math.random() * departments.length)],
          interests: _.sampleSize(interests, Math.floor(Math.random() * interests.length)),
          gender: genders[Math.floor(Math.random() * genders.length)],
          languages: _.sampleSize(languages, Math.floor(Math.random() * languages.length))
        })
      }
      return profiles;
    });
    return _.flatten(profiles);
  })
  .then((profileLists)=> {
    return _.flatten(profileLists);
  });

(async() => {
  try {
    log.info(`Connecting to mongo... ${process.env.MONGO_URL}`);
    var db = await MongoClient.connect(process.env.MONGO_URL);
    log.info('Mongo connected.');
  } catch (err) {
    return log.info('Mongo connectoin error =', err);
  }
  try {
    profiles = await profiles;
    log.info('profiles ok, profiles.length =', profiles.length);
  } catch (err) {
    return log.info('profiles error =', err);
  }
  try {
    log.info(`Inserting documents to mongo... ${process.env.MONGO_URL}`);
    await db.collection('profiles').insertMany(profiles);
    log.info('Documents inserted to mongo.');
  } catch (err) {
    return log.info('Mongo insert error =', err);
  }
  try {
    log.info('Closing mongo...');
    await db.close();
    return log.info('Mongo closed.');
  } catch (err) {
    return log.info('Closing mongo error =', err);
  }
})();
